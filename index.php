<?php
include 'function.php';
include 'cek.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Sistem Pencernaan</a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="penyakit.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-viruses"></i></div>
                            Data Penyakit
                        </a>
                        <a class="nav-link" href="gejala.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-bacterium"></i></div>
                            Data Gejala
                        </a>
                        <a class="nav-link" href="pasien.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-hospital-user"></i></div>
                            Data Pasien
                        </a>
                        <a class="nav-link" href="rekomendasi.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-medkit"></i></div>
                            Rekomendasi
                        </a>
                        <a class="nav-link" href="track.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-notes-medical"></i></div>
                            Track Record Pasien
                        </a>
                        <a class="nav-link" href="admin.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Kelola Admin
                        </a>
                        <a class="nav-link" href="logout.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
                            Logout
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Dashboard</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-primary text-white mb-4"> 
                                <?php 
                                $jumlahpenyakit = mysqli_query($conn, 'SELECT * from penyakit_master');
                                ?>
                                <div class="card-body" align="center"><i class="fas fa-viruses"></i><strong> Jumlah Penyakit</strong></div>
                                <h3 align="center"><?= mysqli_num_rows($jumlahpenyakit) ?></h3>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-warning text-white mb-4">
                                <?php 
                                $jumlahgejala = mysqli_query($conn, 'SELECT * from penyakit_gejala');
                                ?>
                                <div class="card-body" align="center"><i class="fas fa-bacterium"></i><strong> Jumlah Gejala </strong></div>
                                <h3 align="center"><?= mysqli_num_rows($jumlahgejala) ?></h3>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-success text-white mb-4">
                                <?php 
                                $jumlahpasien = mysqli_query($conn, 'SELECT * from pasien_rekam');
                                ?>
                                <div class="card-body" align="center"><i class="fas fa-hospital-user"></i><strong> Jumlah Pasien </strong></div>
                                <h3 align="center"><?= mysqli_num_rows($jumlahpasien) ?></h3>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-danger text-white mb-4">
                                <?php 
                                $jumlahadmin = mysqli_query($conn, 'SELECT * from user');
                                ?>
                                <div class="card-body" align="center"><i class="fas fa-users"></i><strong> Jumlah Admin</strong></div>
                                <h3 align="center"><?= mysqli_num_rows($jumlahadmin) ?></h3>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            <strong>Data Penyakit</strong>
                        </div>
                        <div class="card-body">
                        <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Kode Penyakit</th>
                                            <th>Nama Penyakit</th>
                                            <th>Keterangan</th>
                                            <th>Keluhan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ambilsemuadata = mysqli_query($conn, "select * from penyakit_master");
                                        $no=0;
                                        while ($data = mysqli_fetch_array($ambilsemuadata)) {
                                            $no++;
                                            $id_penyakit = $data['id'];
                                            $kode_penyakit = $data['kode_penyakit'];
                                            $nama_penyakit = $data['nama_penyakit'];
                                            $keterangan = $data['keterangan'];
                                            $keluhan = $data['keluhan'];
                                        ?>
                                            <tr>
                                                <td><?= $kode_penyakit; ?></td>
                                                <td><?= $nama_penyakit; ?></td>
                                                <td><?= $keterangan; ?></td>
                                                <td><?= $keluhan; ?></td>
                                            </tr>
                                            
                                        <?php
                                        };
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
                </div>
            </main>
            
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>
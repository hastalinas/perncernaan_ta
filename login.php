<?php
require 'function.php';

// init variables
$min_number = 1;
$max_number = 15;

// generating random numbers
$random_number1 = mt_rand($min_number, $max_number);
$random_number2 = mt_rand($min_number, $max_number);

if (isset($_POST['login'])) {
    $nameuser = $_POST['nameuser'];
    $passuser = md5($_POST['passuser']);
    $captchaResult = $_POST["captchaResult"];
    $firstNumber = $_POST["firstNumber"];
    $secondNumber = $_POST["secondNumber"];

    $checkTotal = $firstNumber + $secondNumber;
    if ($captchaResult == $checkTotal) {
        //cek login
        $chek = mysqli_query($conn, "SELECT * FROM user WHERE nameuser='$nameuser' AND passuser='$passuser'");

        if (mysqli_num_rows($chek)) {
            $_SESSION['log'] = 'True';
            header('location:index.php');
        } else {
            echo "<script>
             alert('Username atau Password salah')
               </script>";
        }
    } else {
        echo "<script>
             alert('Captha salah')
               </script>";
    }
};

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Login</title>
    <link href="css/styles.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="bg-primary">
    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-5">
                            <div class="card shadow-lg border-0 rounded-lg mt-5">
                                <div class="card-header">
                                    <h3 class="text-center font-weight-light my-4"><strong>Login Sistem Pakar Pencernaan</strong></h3>
                                </div>
                                <div class="card-body">
                                    <form method="post">
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputEmailAddress">Username</label>
                                            <div class="input-group mb-3">
                                                <span class="input-group-text" id="basic-addon1">@</span>
                                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" name="nameuser">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="small mb-1" for="inputPassword">Password</label>
                                            <input class="form-control" name="passuser" id="inputPassword" type="password" placeholder="Enter password" minlength="4" />
                                        </div>

                                        <div class="form-group">
                                            <p>
                                                <?php
                                                echo 'Berapakah ' . $random_number1 . ' + ' . $random_number2 . ' = ';
                                                ?>
                                                <input name="captchaResult" type="text" size="2" onkeypress="return event.charCode >= 48 && event.charCode <=57" />

                                                <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>" />
                                                <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>" />
                                            </p>
                                        </div>

                                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                                            <button class="btn btn-primary" name="login">Login</button>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
</body>

</html>
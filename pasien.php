<?php
include 'function.php';
include 'cek.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data Pasien</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Sistem Pencernaan</a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    </nav>
    
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="penyakit.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-viruses"></i></div>
                            Data Penyakit
                        </a>
                        <a class="nav-link" href="gejala.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-bacterium"></i></div>
                            Data Gejala
                        </a>
                        <a class="nav-link" href="pasien.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-hospital-user"></i></div>
                            Data Pasien
                        </a>
                        <a class="nav-link" href="rekomendasi.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-medkit"></i></div>
                            Rekomendasi
                        </a>
                        <a class="nav-link" href="track.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-notes-medical"></i></div>
                            Track Record Pasien
                        </a>
                        <a class="nav-link" href="admin.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Kelola Admin
                        </a>
                        <a class="nav-link" href="logout.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
                            Logout
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Data Pasien</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Data Pasien</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-header">
                            <!-- Button to Open the Modal -->
                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                Tambah Pasien
                            </button> -->

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pasien</th>
                                            <th>No HP</th>
                                            <th>Create at</th>
                                            <th>Keluhan Utama</th>
                                            <th>Hasil Diagnosa</th>
                                            <th>Negatif</th>
                                            <th>Positif</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ambilsemuadata = mysqli_query($conn, "select * from pasien_rekam");
                                        $no = 0;
                                        while ($data = mysqli_fetch_array($ambilsemuadata)) {
                                            $no++;
                                            $id_pasien = $data['id'];
                                            $nama_pasien = $data['nama_pasien'];
                                            $no_hp = $data['no_hp'];
                                            $create_at = $data['create_at'];
                                            $keluhan_utama = $data['keluhan_utama'];
                                            $hasil_diagnosa = $data['hasil_diagnosa'];
                                            $negatif = $data['negatif'];
                                            $positif = $data['positif'];
                                        ?>
                                            <tr>
                                                <td><?= $no ?></td.>
                                                <td><?= $nama_pasien; ?></td>
                                                <td><?= $no_hp; ?></td>
                                                <td><?= $create_at; ?></td>
                                                <td><?= $keluhan_utama; ?></td>
                                                <td><?= $hasil_diagnosa; ?></td>
                                                <td><?= $negatif; ?></td>
                                                <td><?= $positif; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit<?= $id_pasien; ?>">
                                                        Edit
                                                    </button>
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete<?= $id_pasien; ?>">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            <!-- Edit Modal -->
                                            <div class="modal fade" id="edit<?= $id_pasien; ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Edit Pasien</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <form method="post" enctype="multipart/form-data">
                                                            <div class="modal-body">
                                                            <label class="small mb-1">Nama Pasien</label>
                                                                <input type="text" name="nama_pasien" value="<?= $nama_pasien; ?>" class="form-control" required>
                                                                <br>
                                                                <input type="hidden" name="id_gejala" value="<?= $gejala; ?>">
                                                                <button type="submit" class="btn btn-primary" name="updategejala">Edit</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Delete Modal -->
                                            <div class="modal fade" id="delete<?= $id_pasien; ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Hapus Pasien</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <form method="post">
                                                            <div class="modal-body">
                                                                Apakah Anda Yakin menghapus data <?= $gejala; ?>?
                                                                <input type="hidden" name="id" value="<?= $id_pasien; ?>">
                                                                <br>
                                                                <br>
                                                                <button type="submit" class="btn btn-danger" name="hapuspasien">Hapus</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        };
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>
    <!-- The Modal -->
    <!--     
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
        <div class="modal-content">      
            
            <div class="modal-header">
            <h4 class="modal-title">Tambah Data Pasien</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>           
            
            <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
                <input type="text" name="nama_pasien" placeholder="Nama Pasien" class="form-control" required>
                <br>
                <input type="text" name="keterangan" placeholder="Keterangan" class="form-control" required>
                <br>
                <input type="text" name="keluhan" placeholder="Keluhan" class="form-control" required>
                <br>
                <input type="text" name="saran" placeholder="Saran" class="form-control" required>
                <br>
                <button type="submit" class="btn btn-primary" name="tambahpenyakit">Submit</button>
            </div>
            </form>          
        </div>
        </div>
    </div>
    -->
    
</html>
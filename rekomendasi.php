<?php
include 'function.php';
include 'cek.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data Rekomendasi</title>
    <link href="css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <a class="navbar-brand" href="index.php">Sistem Pencernaan</a>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars"></i></button>
    </nav>
    
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                            Dashboard
                        </a>
                        <a class="nav-link" href="penyakit.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-viruses"></i></div>
                            Data Penyakit
                        </a>
                        <a class="nav-link" href="gejala.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-bacterium"></i></div>
                            Data Gejala
                        </a>
                        <a class="nav-link" href="pasien.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-hospital-user"></i></div>
                            Data Pasien
                        </a>
                        <a class="nav-link" href="rekomendasi.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-medkit"></i></div>
                            Rekomendasi
                        </a>
                        <a class="nav-link" href="track.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-notes-medical"></i></div>
                            Track Record Pasien
                        </a>
                        <a class="nav-link" href="admin.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                            Kelola Admin
                        </a>
                        <a class="nav-link" href="logout.php">
                            <div class="sb-nav-link-icon"><i class="fas fa-sign-out-alt"></i></div>
                            Logout
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Data Rekomendasi</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Data Rekomendasi</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-header">
                            <!-- Button to Open the Modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                                Tambah Rekomendasi
                            </button>

                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Saran</th>
                                            <th>Nama Penyakit</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ambilsemuadata = mysqli_query($conn, "SELECT rekomendasi.id AS id_rekomendasi, penyakit_master.id AS id_master, saran, nama_penyakit, penyakit_master_id
                                        FROM rekomendasi
                                        INNER JOIN penyakit_master
                                        ON rekomendasi.penyakit_master_id=penyakit_master.id");
                                        $no = 0;
                                        while ($data = mysqli_fetch_array($ambilsemuadata)) {
                                            $no++;
                                            $id_rekomendasi = $data['id_rekomendasi'];
                                            $saran = $data['saran'];
                                            $nama_penyakit = $data['nama_penyakit'];
                                            $penyakit_master_id = $data['penyakit_master_id'];
                                        ?>
                                            <tr>
                                                <td><?= $no ?></td.>
                                                <td><?= $saran; ?></td>
                                                <td><?= $nama_penyakit; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#edit<?= $id_rekomendasi; ?>">
                                                        Edit
                                                    </button>
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete<?= $id_rekomendasi; ?>">
                                                        Hapus
                                                    </button>
                                                </td>
                                            </tr>
                                            <!-- Edit Modal -->
                                            <div class="modal fade" id="edit<?= $id_rekomendasi; ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Edit Rekomendasi</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <form method="post" enctype="multipart/form-data">
                                                            <div class="modal-body">
                                                            <label class="small mb-1">Saran</label>
                                                                <input type="text" name="saran" value="<?= $saran; ?>" class="form-control" required>
                                                                <br>
                                                                <select name="penyakit_master_id" class="form-control">
                                                                    <option value="">----PILIH----</option>
                                                                        <?php
                                                                            $sqlPenyakit=mysqli_query($conn, "SELECT * FROM penyakit_master");
                                                                            while($g=mysqli_fetch_array($sqlPenyakit)){
                                                                            echo "<option value='$g[id]'>$g[nama_penyakit]</option>";
                                                                        }
                                                                        ?>
                                                                </select>
                                                                <input type="hidden" name="id_rekomendasi" value="<?= $id_rekomendasi; ?>">
                                                                <br>
                                                                <button type="submit" class="btn btn-primary" name="updaterekomendasi">Edit</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Delete Modal -->
                                            <div class="modal fade" id="delete<?= $id_rekomendasi; ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <!-- Modal Header -->
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Hapus Rekomendasi</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <!-- Modal body -->
                                                        <form method="post">
                                                            <div class="modal-body">
                                                                Apakah Anda Yakin menghapus data rekomendasi penyakit <strong> <?= $nama_penyakit; ?> </strong>?
                                                                <input type="hidden" name="id" value="<?= $id_rekomendasi; ?>">
                                                                <br>
                                                                <br>
                                                                <button type="submit" class="btn btn-danger" name="hapusrekomendasi">Hapus</button>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        };
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>
    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
        <div class="modal-content">      
            <!-- Modal Header -->
            <div class="modal-header">
            <h4 class="modal-title">Tambah Data Rekomendasi</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>           
            <!-- Modal body -->
            <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
            <select name="penyakit_master_id" class="form-control">
                    <option value="">----PILIH----</option>
                        <?php
                            $sqlPenyakit=mysqli_query($conn, "SELECT * FROM penyakit_master");
                            while($g=mysqli_fetch_array($sqlPenyakit)){
                            echo "<option value='$g[id]'>$g[nama_penyakit]</option>";
                        }
                        ?>
                </select>
                <br>
                <input type="text" name="saran" placeholder="Saran" class="form-control" required>
                <br>
                <button type="submit" class="btn btn-primary" name="tambahrekomendasi">Submit</button>
            </div>
            </form>          
        </div>
        </div>
    </div>
</html>